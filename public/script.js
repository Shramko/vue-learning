$(document).ready(function() {
    let usersArr = [];
    let error = '';
    let roles = [];
    let socket = io('/admin-ns');

    socket.on('changeDb', onChangeDb);
    socket.on('changeView', onChangeView);

    // get data from server for page init
    function initPageDataLoad() {
        $.ajax({
            url: "/allusers",
            context: this,
            async: false,
            success: successGetData
        });

        $.ajax({
            url: "/roles",
            context: this,
            async: false,
            success: function(data) {
                roles = data;
            }
        });
    }

    initPageDataLoad();

    /**
     * Component for render list of users
     */
    Vue.component('users-item', {
        props: ['users'],
        template: `
                    <tr>
                        <td class="col-md-3">{{ users.name }}</td>
                        <td class="col-md-3">{{ users.email }}</td>
                        <td class="col-md-3">{{ users.role }}</td>
                        <td class="col-md-1"><button @click="$emit(\'remove\')">Delete</button></td>
                        <td class="col-md-1"><button @click="$emit(\'edit\')">Edit</button></td>        
                    </tr>`,
    });

    let app = new Vue({
        el: '#app',
        data: {
            userId: '',
            userName: '',
            userEmail: '',
            userRole: '',
            userNameError: false,
            userEmailError: false,
            userRoleError: false,
            actionName: '',
            editUserFormSeen: false,
            logFieldSeen: false,
            errors: [],
            success: [],
            title: 'List of users',
            users: usersArr,
            roles: roles,
            badgeCount: 0,
            subscribe: false,
        },
        methods: {

            /**
             * Open form of add/edit user and fill it if this necessary
             *
             * @param {int|null} id
             */
            openEditUserForm: function(id = null) {
                this.editUserFormSeen = true;
                this.errors = [];
                this.success = [];
                let user = [];

                if (id != null) {
                    this.actionName = 'Edit';

                    for (let i = 0; i < this.users.length; i++) {
                        if (this.users[i].id == id) {
                            user = this.users[i];
                        }
                    }

                    if (!user) {
                        this.errors.push('Such user is not exist');
                    } else {
                        this.userId = user.id;
                        this.userName = user.name;
                        this.userEmail = user.email;
                        this.userRole = user.role;
                    }
                } else {
                    this.actionName = 'Add';
                }
            },

            /**
             * Drop user from DB and from view
             *
             * @param {int} id
             * @param {int} index
             */
            dropUser: function(id, index) {
                $.ajax({
                    method: 'POST',
                    url: "/user/delete",
                    context: this,
                    data: {'id': id},
                    complete: function() {
                        console.log('user is del');
                        this.users.splice(index, 1);
                        this.success.push('User is totally deleted');
                        socket.emit('changeDb', `User with id ${id} was successfully deleted`);
                    }
                });
            },

            /**
             * Method who performed if form add/edit user submit
             */
            onSubmit: function() {
                this.errors = [];
                this.success = [];
                this.userNameError = false;
                this.userEmailError = false;
                this.userRoleError = false;
                error = '';

                let id = this.userId;
                let name = '';
                let email = '';
                let role = '';

                /*
                * Validate fields
                * */
                if (validate(this.userName, 'required')) {
                    name = this.userName;
                } else {
                    this.userNameError = error;
                    error = '';
                }

                if (validate(this.userEmail, 'required')
                    && validate(this.userEmail, 'email')) {
                    email = this.userEmail;
                } else {
                    this.userEmailError = error;
                    error = '';
                }

                if (validate(this.userRole, 'required')) {
                    role = this.userRole;
                } else {
                    this.userRoleError = error;
                    error = '';
                }

                if (!this.userNameError
                    && !this.userEmailError
                    && !this.userRoleError) {
                    $.ajax({
                        method: 'POST',
                        url: "/user/edit",
                        context: this,
                        data: {
                            'id': id == undefined ? null : id,
                            'name': name,
                            'email': email,
                            'role': role
                        },
                        complete: function() {
                            initPageDataLoad();
                            this.users = usersArr;

                            if (this.actionName == 'Add') {
                                this.success.push('User is added');
                                socket.emit('changeDb', `${role} with name ${name} was successfully added`);
                            } else {
                                this.success.push('User is changed');
                                socket.emit('changeDb', `${role} with name ${name} was updated`);
                            }
                        }
                    });

                    // clear input fields
                    this.userId = '';
                    this.userName = '';
                    this.userEmail = '';
                    this.userRole = '';
                    this.editUserFormSeen = false;
                }
            },
            unsubscribe: function() {
                if (this.subscribe) {
                    socket.removeAllListeners("changeDb");
                } else {
                    socket.on('changeDb', onChangeDb);
                }
            }
        }
    });

    /**
     * Callback function to ajax request for get all users
     *
     * @param {Array} data array with users
     */
    function successGetData(data) {
        usersArr = data;
    }

    /**
     * Validate data by validation type
     *
     * @param {string} data
     * @param {string} type
     * @returns {boolean}
     */
    function validate(data, type) {
        switch (type) {
            case 'required':
                if (data.length == 0) {
                    error = 'Field can not be empty';
                    return false;
                }
                break;
            case 'email':
                let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!pattern.test(data)) {
                    error = 'Mail format is not valid';
                    return false;
                }
                break;
        }

        return true;
    }

    /**
     * Callback function for socket event 'changeDb'
     *
     * @param {string} data
     */
    function onChangeDb(data) {
        $('#logField').append(`<div>${data}</div>`);
        app.badgeCount++;
    }

    /**
     * Callback function for socket event 'changeView'
     */
    function onChangeView() {
        initPageDataLoad();
        app.users = usersArr;
    }
});