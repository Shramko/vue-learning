'use strict';

const express = require('express');
let mysql = require('mysql');
let fs = require('fs');
let bodyParser = require('body-parser')

// Host params
const PORT = 8080;
const HOST = '0.0.0.0';

// query for get all users
let sql = `SELECT * FROM users`;

let users;

// Connection to DB
let con = mysql.createConnection({
    host: "db",
    user: "root",
    password: "root",
    database: "nodedb"
});

con.connect(function(err) {
    if (err) throw err;

    con.query(sql, function(err, result) {
        if (err) throw err;
        users = result;
    });
});

const app = express();
let server = require('http').Server(app);
let io = require('socket.io')(server);

server.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

app.use(express.static('public'));
app.use(express.static(__dirname));

let adminsNamespace = io.of('/admin-ns');

adminsNamespace.on('connection', function(socket) {

    socket.on('changeDb', function(data) {
        adminsNamespace.emit('changeDb', data);
        socket.broadcast.emit('changeView');
    });
});

// user delete
app.post('/user/delete', (req, res) => {
    con.query(`DELETE FROM users WHERE id = "${req.body.id}";`, function(err, result) {
        if (err) throw err;

        res.send(result);
    });
});

app.post('/user/edit', (req, res) => {
    // console.log('In edit page');
    if (req.body.id) {
        con.query(`UPDATE users SET name = "${req.body.name}", email = "${req.body.email}", role = "${req.body.role}"
         WHERE id = ${req.body.id};`, function(err, result) {
            if (err) throw err;

            res.send(result);
        });
    } else {
        con.query(`INSERT INTO users (name, email, role) VALUES ("${req.body.name}", "${req.body.email}",
         "${req.body.role}");`, function(err, result) {
            if (err) throw err;

            res.send(result);
        });
    }
});

// get all users
app.get('/allusers', (req, res) => {
    // console.log('In /allusers page');
    con.query(sql, function(err, result) {
        if (err) throw err;
        users = result;

        res.send(users);
    });
});

// get all roles
app.get('/roles', (req, res) => {
    // console.log('In /roles page');
    con.query(`SELECT DISTINCT role FROM users`, function(err, result) {
        if (err) throw err;
        let roles = [];

        for (let i = 0; i < result.length; i++) {
            roles.push(result[i].role);
        }

        res.send(roles);
    });
});